class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates :lines, :numericality => { :greater_than => 0, :less_than_or_equal_to => 100, :message => 
                                          "Number of users must be grater than 0 and less than 100" }
  validates :phone, format: { with: /\d{3}\d{3}\d{4}/, message: "bad format" }
  validates :whichmodel, :presence => true
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
