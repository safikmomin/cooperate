class SubscribersController < ApplicationController
    before_filter :authenticate_user!
    
    def new
        if current_user.whichmodel == "Business"
            if current_user.lines < 11
                planes = 101
            else
                planes = 102
            end
        else
            planes = 103
        end
    end
    
    def create
        if current_user.whichmodel == "Business"
            if current_user.lines < 11
                planes = 101
            else
                planes = 102
            end
        else
            planes = 103
        end
        
        @code = params[:couponCode]
        
        #gets the credit card details submitted in the form
        token = params[:stripeToken]
        
        if params[:couponCode] == ""
            customer = Stripe::Customer.create(
              card: token,
              quantity: current_user.lines,
              plan: planes,
              email: current_user.email
            )
        else
            #create a customer
            customer = Stripe::Customer.create(
              card: token,
              quantity: current_user.lines,
              plan: planes,
              coupon: @code,
              email: current_user.email
            )
        end
        
        current_user.subscribed = true
        current_user.stripeid = customer.id
        current_user.save
    
    
        redirect_to user_path(current_user.id), notice: "Your subscription was set up successfully!"
        
    end
    
    
    def index
    end
    
end
