class AddPhoneAndLinesToUsers < ActiveRecord::Migration
  def change
    
    add_column :users, :lines, :integer,  default: 1,   null: false
    add_column :users, :phone, :string,  default: "",   null: false
    add_column :users, :name, :string,    default: "",  null: false
    add_column :users, :whichmodel, :string,    default: "",  null: false
  end
end
